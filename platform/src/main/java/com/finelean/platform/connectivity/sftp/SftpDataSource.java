/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */
package com.finelean.platform.connectivity.sftp;

import com.jcraft.jsch.*;
import org.apache.log4j.Logger;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * This is the sftp data source.
 * The class initializes the sftp session.
 *
 * @author sso
 */
public class SftpDataSource {

    private static final Logger logger = Logger.getLogger(SftpDataSource.class);

    private SessionContext sessionContext;

    private SessionTemplate sessionTemplate;

    private RetryTemplate retryTemplate;

    @PostConstruct
    private void init() throws Exception {
        doCreateSession();
    }

    /**
     * Keeps the server alive, in case the firewall terminates the session.
     * It sends a keep alive message to the sftp every 1 minute.
     *
     */
    @Scheduled(initialDelay = 10000, fixedRate = 60000)
    private void keepServerAlive() {
        try {
            doSendKeepAliveMessage();
            logger.debug("session keep alive message sent");
        } catch (Exception e) {
            try {
                retryTemplate.execute(new RetryCallback<Void>() {
                    @Override
                    public Void doWithRetry(RetryContext context) throws Exception {
                        doCreateSession();
                        doSendKeepAliveMessage();
                        return null;
                    }
                });
            } catch (Exception ee) {
                logger.warn("Session beyond repair.", ee);
            }
        }

    }

    /**
     *
     * @return a sftp channel from the active Jsch session
     * @throws Exception
     */
    public ChannelSftp getSftpChannel() throws Exception {

        ChannelSftp sftpChannel;

        try {
            sftpChannel = doOpenSftpChannel();
        } catch (Exception ex) {
            logger.warn("Problem with the jsch session.  Retrying...", ex);
            sftpChannel = retryTemplate.execute(
                    new RetryCallback<ChannelSftp>() {
                        @Override
                        public ChannelSftp doWithRetry(final RetryContext context) throws Exception {
                            doCreateSession();
                            logger.warn("Retrying #" + context.getRetryCount());
                            return doOpenSftpChannel();
                        }
                    });
        }

        return sftpChannel;

    }

    public String getRemoteBaseDir() {
        return sessionContext.getRemoteBaseDir();
    }

    /**
     * Sends a keep alive message.
     * The session has to be safe-guarded by a lock, in case
     * the session is in the process of changing hands in doCreateSession().
     *
     * @throws Exception
     */
    private void doSendKeepAliveMessage() throws Exception {

        sessionTemplate.execute(new SessionCallback<Void>() {
            @Override
            public String getName() {
                return "keep alive";
            }

            @Override
            public Void execute() throws Exception {
                sessionContext.getSession().sendKeepAliveMsg();
                return null;
            }
        });
    }


    private void doCreateSession() throws Exception {

        sessionTemplate.execute(new SessionCallback<Object>() {
            @Override
            public String getName() {
                return "create session";
            }

            @Override
            public Object execute() throws Exception {
                sessionContext.createSession();
                return null;

            }
        });

    }

    private ChannelSftp doOpenSftpChannel() throws Exception {

        return sessionTemplate.execute(new SessionCallback<ChannelSftp>() {
            @Override
            public String getName() {
                return "open sftp channel";
            }

            @Override
            public ChannelSftp execute() throws Exception {
                Session s = sessionContext.getSession();

                if (s == null) {
                    throw new Exception("why would this be null?");
                }

                return (ChannelSftp) s.openChannel("sftp");
            }
        });

    }

    @PreDestroy
    private void destroy() throws Exception {

        sessionTemplate.execute(new SessionCallback<Void>() {
            @Override
            public String getName() {
                return "destroy";
            }

            @Override
            public Void execute() throws Exception {
                sessionContext.getSession().disconnect();
                return null;
            }
        });
    }

    public SessionContext getSessionContext() {
        return sessionContext;
    }

    public void setSessionContext(SessionContext sessionContext) {
        this.sessionContext = sessionContext;
    }

    public SessionTemplate getSessionTemplate() {
        return sessionTemplate;
    }

    public void setSessionTemplate(SessionTemplate sessionTemplate) {
        this.sessionTemplate = sessionTemplate;
    }

    public RetryTemplate getRetryTemplate() {
        return retryTemplate;
    }

    public void setRetryTemplate(RetryTemplate retryTemplate) {
        this.retryTemplate = retryTemplate;
    }
}

