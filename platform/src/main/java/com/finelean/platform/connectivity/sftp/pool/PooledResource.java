/*
 * Copyright 2014 Simon So
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */
package com.finelean.platform.connectivity.sftp.pool;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;
import com.finelean.platform.connectivity.sftp.SftpDataSource;
import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * This class is the heart-and-soul unit of the sftp channel operation.
 * It is wrapped as such so that it represents a pooled resource of the connection pool.
 *
 * @author sso
 */
public class PooledResource {

    private static final Logger logger = Logger.getLogger(PooledResource.class);

    private static final String JSCH_ERR_MSG_NO_SUCH_FILE = "No such file";

    SftpDataSource sftpDataSource;

    private ChannelSftp sftpChannel;


    /**
     * Initializes the resources.
     * Don't put the logic in the constructor; sftpDataSource haven't been finished initializing.
     *
     * @throws Exception if we are unable to initialize from the beginning,
     *                   just panic and quit the bean creation process.
     */
    @PostConstruct
    private void init() throws Exception {

        sftpChannel = sftpDataSource.getSftpChannel();
        if (sftpChannel == null) {
            throw new Exception("Problem obtaining a sftp channel.  Please check with the configuration.");
        }
        sftpChannel.connect();

        String remoteBaseDir = sftpDataSource.getRemoteBaseDir();

        try {
            // to ensure the root is created.
            doGetAttrs(remoteBaseDir);
        } catch (SftpException se) {
            if (JSCH_ERR_MSG_NO_SUCH_FILE.equals(se.getMessage())) {
                sftpChannel.mkdir(remoteBaseDir);
            }
        }
    }

    /**
     * Must implement this one to ensure the resources are freed!
     *
     * The commons-pool manages the lifecycle of the pooled objects
     */
    @PreDestroy
    private void destroy() {
        if (sftpChannel != null) {
            logger.debug("destroying the pooled resource...");
            sftpChannel.disconnect();
        }
    }

    boolean isValid() {
        return sftpChannel.isConnected();
    }

    /**
     * API models after #java.nio.file.Files
     *
     * @param path path to detect
     * @return true if the directory exists.
     * @throws Exception
     * @see java.nio.file.Files
     */
    public boolean isDirectory(final String path) {

        if (StringUtils.isEmpty(path)) {
            return false;
        }

        resetToRemoteBaseDir();

        return doIsDirectory(path);
    }

    private boolean doIsDirectory(String path) {
        boolean isDirectory = false;

        try {
            SftpATTRS sftpATTRS = doGetAttrs(path);
            isDirectory = (sftpATTRS != null && sftpATTRS.isDir());
        } catch (Exception e) {
            logger.debug("directory doesn't exists: " + path);
        }

        logger.debug(path + " isDirectory: " + isDirectory);
        return isDirectory;
    }


    public boolean isFile(final String path) {

        if (StringUtils.isEmpty(path)) {
            return false;
        }

        resetToRemoteBaseDir();

        boolean retVal = false;

        try {
            SftpATTRS sftpATTRS = doGetAttrs(path);
            if (sftpATTRS != null && sftpATTRS.isReg()) {
                retVal = true;
            }

        } catch (Exception e) {
            if (e instanceof SftpException) {
                if (JSCH_ERR_MSG_NO_SUCH_FILE.equals(e.getMessage())) {
                    logger.debug(e.getMessage());
                    return false;
                }
            } else {
                logger.error("can't determine whether it's a file: ", e);
            }
        }

        return retVal;
    }

    private SftpATTRS doGetAttrs(final String path) throws Exception {
        return sftpChannel.lstat(path);
    }

    /**
     * API models after #java.nio.file.Files
     *
     * @param path the path the file resides
     * @param filename the filename
     * @param srcInputStream the content to be written
     * @return true if successful
     * @throws Exception
     * @see java.nio.file.Files
     */
    public boolean write(final String path,
                         final String filename,
                         InputStream srcInputStream) {

        resetToRemoteBaseDir();

        boolean retVal = false;

        try {
            doRecursiveCreateDir(path);
        } catch (Exception e) {
            logger.error("Unable to create directory: ", e);
            return retVal;
        }

        try (OutputStream outputStream = (srcInputStream == null)
                ? sftpChannel.put(filename, ChannelSftp.OVERWRITE)
                : null) {

            if (outputStream == null) {
                sftpChannel.put(srcInputStream, filename);
            }

            logger.debug("Created file at: " + path);
            retVal = true;

        } catch(Exception e) {
            logger.warn("Unable to create file: " + path, e);
        }

        return retVal;

    }

    public boolean delete(final String path, final String filename) {

        boolean retVal = false;

        try {
            resetToRemoteBaseDir();
            doDelete(path, filename);
            retVal = true;
        } catch (Exception e) {
            logger.error("Unable to delete: " + path + "/" + filename, e);
        }

        return retVal;

    }

    /**
     *
     * @param path
     * @param nextInPath if *, delete all contents in there.
     * @throws SftpException
     */
    private void doDelete(String path, String nextInPath) throws SftpException {

        // recursively go down from path
        int numLevels = 0;

        try {
            String [] levels = path.split("/");
            for (String level : levels) {
                sftpChannel.cd(level);
                numLevels ++;
            }

            if (!"*".equals(nextInPath) && doIsDirectory(nextInPath)) {
                List<String> dirContents = doDirectoryList(nextInPath);
                for (String dirContent : dirContents) {
                    // need to empty out directory before deleting the directory
                    doDelete(nextInPath, dirContent);
                }
                sftpChannel.rmdir(nextInPath);
            } else {
                // remove all files in path
                if ("*".equals(nextInPath)) {
                    List<String> dirContents = doDirectoryList(".");
                    for (String dirContent : dirContents) {
                        if (!doIsDirectory(dirContent)) {
                            sftpChannel.rm(dirContent);
                        }
                    }
                } else {
                    sftpChannel.rm(nextInPath);
                }
            }
        } finally {
            while (numLevels > 0) {
                sftpChannel.cd("..");
                numLevels --;
            }
        }
    }

    @SuppressWarnings("rawtypes")
    public List<String> ls(final String path) {
        resetToRemoteBaseDir();
        return doDirectoryList(path);
    }

    @SuppressWarnings("unchecked")
    private List<String> doDirectoryList(String path) {
        List<String> directories = new ArrayList<>();

        try {
            Vector<ChannelSftp.LsEntry> files = sftpChannel.ls(path);

            for (ChannelSftp.LsEntry file : files) {
                String f = file.getFilename();
                switch (f) {
                    case ".":
                    case "..":
                        continue;
                    default:
                        directories.add(f);
                        continue;
                }
            }
        } catch (Exception e) {
            logger.error("Exception: ", e);
        }
        return directories;
    }

    private void resetToRemoteBaseDir() {
        String remoteBaseDir = sftpDataSource.getRemoteBaseDir();
        try {
            String home = sftpChannel.getHome();
            sftpChannel.cd(home + "/" + remoteBaseDir);
        } catch (Exception e) {
            logger.error("Unable to initialize the pooled resource: ", e);
        }
    }

    /**
     * Recursively creates levels of directory.
     *
     * @param path
     * @throws Exception handles at the caller.
     */
    private void doRecursiveCreateDir(final String path) throws Exception {
        String [] subDirs = path.split("/");
        SftpATTRS sftpATTRS = null;
        for (String subDir : subDirs) {
            try {
                sftpATTRS = doGetAttrs(subDir);
            } catch (Exception e) {
                // Don't like this old style API.  Should not have the caller
                // to handle exception if directory not found!
                // JSCH_NOT_FOUND is e.getMessage()
                sftpChannel.mkdir(subDir);
            }
            sftpChannel.cd(subDir);
        }
    }

    public SftpDataSource getSftpDataSource() {
        return sftpDataSource;
    }

    public void setSftpDataSource(SftpDataSource sftpDataSource) {
        this.sftpDataSource = sftpDataSource;
    }
}
